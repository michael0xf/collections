package mtk.collections;


import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;

/**
 * @author <a href="mailto:mtkravchenko@gmail.com">Mikhail Kravchenko</a>
 */

public class UTable<T1, T2> implements Map<T1, T2> {
    protected UniqueArray2<T1> code;
    protected Array<T2> value;

    public UTable() {
        table(32);
    }

    public UTable(int size) {
        table(size);
    }

    public void table(int size) {
        code = new UniqueArray2<T1>(size);
        value = new Array<T2>(size);
    }

    public void clear() {
        code().clear();
        value().clear();
    }

    public UniqueArray2 code() {
        return code;
    }

    public Array value() {
        return value;
    }

    public boolean containsKey(Object key) {
        return code.indexOfObject(key) >= 0;
    }

    @Override
    public boolean containsValue(Object value) {
        return this.value.contains(value);
    }

    public Enumeration elements() {
        return value.elements();
    }

    public T2 get(int c) {
        int index = code.findMiddle(c);
        if (index < code.amount) {
            if (code.arr[index].hashCode() == c)
                return value.elementAt(index);
        }
        return null;
    }

    public T2 get(Object key) {
        int index = code.indexOfObject(key);
        if (index >= 0)
            return value.elementAt(index);
        return null;
    }

    public boolean isEmpty() {
        return value.size() == 0;
    }

    public T2 put(T1 key, T2 val) {
        int index = code.put(key);
        if (code.size() == value.size()) {

            T2 ret = value.elementAt(index);
            value.setElementAt(val, index);
            return ret;
        } else if (code.size() == value().size() + 1) {
            value.insertElementAt(val, index);
        } else {
            System.err.println("HTable T put(T key, T val)");

        }
        return null;

    }

    @Override
    public T2 remove(Object key) {
        int index = code.removeElement(key);
        return value.removeElementAt(index);
    }

    @Override
    public void putAll(Map<? extends T1, ? extends T2> m) {
        code.expand(size() + m.size());
        value.expand(size() + m.size());
        for (Entry<? extends T1, ? extends T2> entry : m.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    public int size() {
        return code.size();
    }


    @Override
    public Set<T1> keySet() {
        return code;
    }

    @Override
    public Collection<T2> values() {
        return value;
    }

    @Override
    public Set<Map.Entry<T1, T2>> entrySet() {
        UniqueArray2<Map.Entry<T1, T2>> ret = new UniqueArray2<>(size());
        UniqueArray2<T1> code = this.code;
        Array<T2> v = this.value;
        for (int i = 0; i < size(); i++) {
            ret.add(new Entry2(code.elementAt(i), v.elementAt(i)));
        }
        return ret;
    }

    class Entry2 implements Map.Entry<T1, T2> {
        private final T1 key;
        private T2 value;

        public Entry2(T1 key, T2 value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public T1 getKey() {
            return key;
        }

        @Override
        public T2 getValue() {
            return value;
        }

        @Override
        public T2 setValue(T2 v) {
            T2 ret = value;
            this.value = v;
            return ret;
        }

    }

}
