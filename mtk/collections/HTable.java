package mtk.collections;


import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;

/**
 * @author <a href="mailto:mtkravchenko@gmail.com">Mikhail Kravchenko</a>
 */
public class HTable<T0, T1> implements Map<T0, T1> {

    protected HashArray<T0> code;
    protected Array<T1> value;
    class Entry2 implements Map.Entry<T0, T1> {
        private final T0 key;
        private T1 value;

        public Entry2(T0 key, T1 value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public T0 getKey() {
            return key;
        }

        @Override
        public T1 getValue() {
            return value;
        }

        @Override
        public T1 setValue(T1 v) {
            T1 ret = value;
            this.value = v;
            return ret;
        }

    }

    public HTable(int size) {
        table(Object.class, Object.class, size);
    }

    public HTable(Class t0, Class t1, int size) {
        table(t0, t1, size);
    }

    public void table(Class t0, Class t1, int size) {
        code = new HashArray<T0>(t0, size);
        value = new Array<T1>(t1, size);
    }

    public void clear() {
        code().clear();
        value().clear();
    }


    public T0 keyAt(int i) {
        return code.elementAt(i);
    }

    public T1 pop() {
        if (code.size() > 0) {
            code.pop();
            return value.pop();
        }
        return null;
    }

    public T0 popKey() {
        if (code.size() > 0) {
            value.pop();
            return code.pop();
        }
        return null;
    }

    public T1 valueAt(int i) {
        return value.elementAt(i);
    }

    public HashArray<T0> code() {
        return code;
    }

    public Array<T1> value() {
        return value;
    }


    public boolean containsKey(Object key) {
        return code.indexOfObject(key) >= 0;
    }

    public boolean contains(Object key) {
        return containsKey(key);
    }

    public Enumeration elements() {
        return value.elements();
    }


    public T1 get(Object key) {
        int index = code.indexOfObject(key);
        if (index >= 0)
            return value.elementAt(index);
        return null;
    }

    public boolean isEmpty() {
        return value.size() == 0;
    }


    public T1 put(Object key, Object val) {
        //int index = code.indexOfObject(key);
        int a0 = code.amount;
        int index = code.put(key);
        if ((index < value.size()) && (a0 == code.amount)) {
            T1 ret = value.elementAt(index);
            value.setElementAt(val, index);
            return ret;
        }
        value.insertElementAt(val, index);
        return null;
    }

    public T1 remove(Object key) {
        int index = code.removeElement(key);
        return value.removeElementAt(index);
    }

    public int size() {
        return code.size();
    }

    @Override
    public boolean containsValue(Object value) {
        return this.value.contains(value);
    }

    @Override
    public Set<T0> keySet() {
        return code;
    }

    @Override
    public Collection<T1> values() {
        return value;
    }

    @Override
    public Set<Map.Entry<T0, T1>> entrySet() {
        HashArray<Map.Entry<T0, T1>> ret = new HashArray<>(size());
        HashArray<T0> code = this.code;
        Array<T1> v = this.value;
        for (int i = 0; i < size(); i++) {
            ret.add(new HTable.Entry2(code.elementAt(i), v.elementAt(i)));
        }
        return ret;
    }

    @Override
    public void putAll(Map<? extends T0, ? extends T1> m) {
        code.expand(size() + m.size());
        value.expand(size() + m.size());
        for (Entry<? extends T0, ? extends T1> entry : m.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

}
