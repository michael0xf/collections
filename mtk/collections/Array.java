package mtk.collections;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

/**
 * @author <a href="mailto:mtkravchenko@gmail.com">Mikhail Kravchenko</a>
 */
public class Array<T extends Object> implements List<T> {

    public static final int NIL = -1;
    public int amount = 0;
    public T[] arr;
    Class c;

    public Array(Class c) {
        this.c = c;
        array(32);
    }

    public Array() {
        this.c = Object.class;
        array(32);
    }

    public Array(int size) {
        this.c = Object.class;
        array(size);
    }

    public Array(Class c, int size) {
        this.c = c;
        array(size);
    }

    public Array(T[] obj) {
        arr = obj;
    }


    public Array(T[] obj, int amount) {
        arr = obj;
        this.amount = amount;
    }

    public static int find2(final Object[] arr, final Object obj, final int amount) {
        for (int i = 0; i < amount; i++) {
            Object o = arr[i];
            if (o == null) {
                if (obj == null)
                    return i;
            } else if (o.equals(obj))
                return i;
        }
        return NIL;
    }

    public static int find3(final Object[] arr, final Object obj, final int amount) {
        for (int i = amount - 1; i >= 0; i--) {
            Object o = arr[i];
            if (o == null) {
                if (obj == null)
                    return i;
            } else if (o.equals(obj))
                return i;
        }
        return NIL;
    }

    public static int find(final Object[] arr, final Object obj, final int amount) {
        for (int i = 0; i < amount; i++) {
            if (arr[i].equals(obj))
                return i;
        }
        return NIL;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < amount; i++) {
            if (o.equals(arr[i]))
                return i;
        }
        return -1;
    }

    public T[] newArray(int amount) {
        return (T[]) java.lang.reflect.Array.newInstance(c, amount);
    }

    public T[] cloneArray() {

        T[] newArr = newArray(amount);
        System.arraycopy(arr, 0, newArr, 0, amount);
        return newArr;

    }

    public void removeAllElements() {
        amount = 0;
    }

    public int copyTo(Array buf) {
        return buf.copyFrom(arr, 0, 0, amount);
    }

    public void copyTo(T[] buf) {
        System.arraycopy(arr, 0, buf, 0, amount);
    }

    public int copyFrom(T[] from, int offset, int toindex, int len) {
        if (len == 0)
            return 0;
        if (offset >= from.length)
            return 0;
        if (offset + len > from.length) {
            len = from.length - offset;
        }
        int amount = this.amount;
        T[] arr = this.arr;
        final int newlen = toindex + len;
        if (newlen > arr.length) {
            int len2 = newlen;
            if (len2 < arr.length << 1)
                len2 = arr.length << 1;
            final T[] newArr = newArray(len2);
            if (toindex >= amount)
                System.arraycopy(arr, 0, newArr, 0, amount);
            else
                System.arraycopy(arr, 0, newArr, 0, toindex);
            arr = newArr;
            this.arr = arr;
        }
        if (newlen > amount) {
            amount = toindex + len;
            this.amount = amount;
        }
        System.arraycopy(from, offset, arr, toindex, len);
        return len;
    }

    public int insertArray(final Object[] from, final int offset, final int toindex, int len) {
        if (len == 0)
            return 0;
        if (offset >= from.length)
            return 0;
        if (offset + len > from.length) {
            len = from.length - offset;
        }
        int amount = this.amount;
        T[] arr = this.arr;
        int newlen;
        if (toindex <= amount)
            newlen = amount + len;
        else
            newlen = toindex + len;
        if (newlen > arr.length) {
            int len2 = newlen;
            if (len2 < arr.length << 1)
                len2 = arr.length << 1;
            final T[] newArr = newArray(len2);
            if (toindex < amount) {
                System.arraycopy(arr, 0, newArr, 0, toindex);
                System.arraycopy(arr, toindex, newArr, toindex + len, amount - toindex);
            } else
                System.arraycopy(arr, 0, newArr, 0, amount);
            arr = newArr;
            this.arr = arr;
        } else if (toindex < amount)
            System.arraycopy(arr, toindex, arr, toindex + len, amount - toindex);
        System.arraycopy(from, offset, arr, toindex, len);
        amount = newlen;
        this.amount = amount;
        return len;
    }

    public int clearFromAt(int begin) {
        int amount = this.amount;
        if (begin < amount) {
            int ret = amount - begin;
            amount = begin;
            this.amount = amount;
            return ret;
        } else
            return 0;
    }

    public T lastElement() {
        int amount = this.amount;
        if (amount > 0)
            return arr[amount - 1];
        else return null;
    }

    private boolean addAll(Object[] src, int amountSrc) {
        int amount0 = amount;
        expand(amount0 + amountSrc);
        System.arraycopy(src, 0, arr, amount0, amountSrc);
        amount = amount0 + amountSrc;
        return true;
    }

    public java.util.Enumeration elements() {

        return new Enumerator();
    }

    public void array(int size) {
        arr = newArray(size);
        amount = 0;
    }

    public int size() {
        return amount;
    }

    public T elementAt(int i) {
        if (i >= amount)
            return null;
        try {

            return arr[i];
        } catch (java.lang.ArrayIndexOutOfBoundsException e) {
            return null;
        } finally {

        }
    }

    public T firstElement() {
        if (amount > 0)
            return arr[0];
        else
            return null;
    }

    public void clear() {
        amount = 0;
    }

    @Override
    public T get(int index) {
        return elementAt(index);
    }

    @Override
    public T set(int index, T element) {
        return setElementAt(element, index);
    }

    @Override
    public void add(int index, T element) {
        insertElementAt(element, index);
    }

    @Override
    public T remove(int index) {
        return removeElementAt(index);
    }

    public int findLastIndexOfRange(int index, Object o) {
        while ((index < amount - 1) && (arr[index + 1] == o)) {
            index++;
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index = indexOf(o);
        if (index < 0)
            return index;
        return findLastIndexOfRange(index, o);
    }

    @Override
    public java.util.ListIterator<T> listIterator() {
        return new Enumerator();
    }

    @Override
    public java.util.ListIterator<T> listIterator(int index) {
        return new Enumerator(index);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        int len = toIndex - fromIndex;
        if (toIndex > amount || len < 0 || fromIndex < 0) {
            throw new IndexOutOfBoundsException();
        }
        Array ret =  new Array<T>(len);
        if (len > 0) {
            ret.copyFrom(arr, fromIndex, 0, len);
        }
        return ret;
    }

    @Override
    public Spliterator<T> spliterator() {
        return List.super.spliterator();
    }

    @Override
    public Stream<T> stream() {
        return List.super.stream();
    }

    @Override
    public Stream<T> parallelStream() {
        return List.super.parallelStream();
    }

    public int getMaxBlockLength() {
        return 65536;
    }

    public int addElement(final T obj) {
        int amount = this.amount;
        int newAmount = amount + 1;
        expand();
        this.arr[amount] = obj;
        this.amount = newAmount;
        return newAmount;
    }

    public void reverse(final int index, final int newindex) {
        final int amount = this.amount;
        final T[] arr = this.arr;

        if ((index < 0) || (index >= amount))
            return;
        if ((newindex >= amount) || (newindex < 0))
            return;

        final T obj = arr[index];
        if (newindex < index) {
            final int count = index - newindex;
            System.arraycopy(arr, newindex, arr, newindex + 1, count);
            arr[newindex] = obj;
        } else if (index < newindex) {
            final int count = newindex - index;
            if (count > 0)
                System.arraycopy(arr, index + 1, arr, index, count);
            arr[newindex] = obj;
        }
    }

    public int luxuriance() {
        return arr.length;
    }

    protected void expandTo(int index) {
        T[] arr = this.arr;
        if ((arr != null) && (index >= arr.length)) {
            int diff = index - arr.length + 1;
            if (diff < getMaxBlockLength() && (diff < arr.length))
                diff = arr.length;
            expand(arr.length + diff);
        } else
            expand();
    }

    public void insertElementAt(final Object obj, final int index) {
        expandTo(index);
        T[] arr = this.arr;
        int amount = this.amount;
        if (index < amount)
            System.arraycopy(arr, index, arr, index + 1, amount - index);
        arr[index] = (T) obj;
        amount++;
        if (amount <= index)
            amount = index + 1;
        this.amount = amount;
    }

    public T removeElementAt(final int index) {
        int amount = this.amount;
        final T[] arr = this.arr;
        T obj = null;
        if ((index >= 0) && (index < amount)) {
            obj = arr[index];
            if (amount - index - 1 > 0)
                System.arraycopy(arr, index + 1, arr, index, amount - index - 1);
            amount--;
            this.amount = amount;
        }
        return obj;
    }

    public void removeUpTo(int index) {
        removeElementsFrom(0, index + 1);
    }

    public void removeElementsFrom(final int index, int len) {
        int amount = this.amount;
        final T[] arr = this.arr;
        if ((index >= 0) && (index < amount)) {
            if (index + len > amount)
                len = amount - index;
            if (amount - index - len > 0)
                System.arraycopy(arr, index + len, arr, index, amount - index - len);
            amount -= len;
            this.amount = amount;

        }
    }

    protected void expand(int newSize) {
        final T[] newArr = newArray(newSize);
        if (arr != null) {
            int amount = this.amount;
            if (amount > 0) {
                if (amount > newSize) {
                    amount = newSize;
                    this.amount = amount;
                }
                System.arraycopy(this.arr, 0, newArr, 0, amount);
            }
        }
        this.arr = newArr;

    }

    protected void expand() {
        T[] arr = this.arr;
        int additionalSize;
        int size;
        if (arr == null) {
            size = 0;
            additionalSize = 2;
            amount = 0;
        } else {
            size = arr.length;
            if (size > amount)
                return;
            additionalSize = size;
        }
        if (additionalSize > getMaxBlockLength())
            additionalSize = getMaxBlockLength();
        expand(size + additionalSize);
    }

    public int removeElement(final Object obj) {
        int i = find3(arr, obj, amount);
        if (i >= 0)
            removeElementAt(i);
        return i;
    }

    public T pop() {
        int amount = this.amount;
        if (amount <= 0)
            return null;
        amount--;
        this.amount = amount;
        return arr[amount];

    }

    public T setElementAt(final Object obj, final int index) {
        expandTo(index);
        int amount = this.amount;
        T[] arr = this.arr;
        T ret = null;
        if (index >= amount) {
            for (int i = amount; i < index; i++)
                arr[i] = null;
            amount = index + 1;
            this.amount = amount;
        } else
            ret = arr[index];
        arr[index] = (T) obj;
        return ret;

    }

    public T[] toArray() {
        T[] t = newArray(amount);
        copyTo(t);
        return t;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        System.arraycopy(arr, 0, a, 0, amount);
        return a;
    }

    @Override
    public boolean add(T t) {
        return addElement(t) >= 0;
    }

    @Override
    public boolean remove(Object o) {
        return removeElement(o) >= 0;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return addAll(c.toArray(), c.size());
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        insertArray(c.toArray(), 0, index, c.size());
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        UniqueArray2 buf = new UniqueArray2();
        buf.addAll(c);
        int amount = this.amount;
        Object[] arr = this.arr;
        for (int i = 0; i < amount; i++) {
            Object o = arr[i];
            if (buf.contains(o)) {
                if (i < amount - 1) {
                    System.arraycopy(arr, i + 1, arr, i, arr.length - i - 1);
                } else {
                    amount = amount - 1;
                    this.amount = amount - 1;
                }
            }
        }
        return true;
    }

    @Override
    public boolean removeIf(Predicate<? super T> filter) {
        return List.super.removeIf(filter);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        UniqueArray2 buf = new UniqueArray2();
        buf.addAll(c);
        int amount = this.amount;
        Object[] arr = this.arr;
        for (int i = 0; i < amount; i++) {
            Object o = arr[i];
            if (!buf.contains(o)) {
                if (i < amount - 1) {
                    System.arraycopy(arr, i + 1, arr, i, arr.length - i - 1);
                } else {
                    amount = amount - 1;
                    this.amount = amount - 1;
                }
            }
        }
        return true;
    }

    @Override
    public void replaceAll(UnaryOperator<T> operator) {
        List.super.replaceAll(operator);
    }

    @Override
    public void sort(Comparator<? super T> c) {
        List.super.sort(c);
    }

    public int offset() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return amount == 0;
    }

    @Override
    public java.util.Iterator<T> iterator() {
        return new Enumerator();

    }

    @Override
    public void forEach(Consumer<? super T> action) {
        List.super.forEach(action);
    }

    class Enumerator implements java.util.Enumeration, java.util.ListIterator<T>, java.util.Iterator<T> {

        int index;

        Enumerator() {
            index = 0;
        }
        Enumerator(int index){
            this.index = index;
        }

        public boolean hasMoreElements() {
            return index < amount;
        }

        public T nextElement() {
            return arr[index++];
        }

        public void reset() {
            index = 0;
        }

        @Override
        public boolean hasNext() {
            return index >= 0 && index < amount - 1;
        }

        @Override
        public T next() {
            int nextIndex = index + 1;
            if (nextIndex < amount - 1) {
                return arr[nextIndex];
            }
            return null;
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public T previous() {
            int nextIndex = index - 1;
            if (nextIndex > 0) {
                return arr[nextIndex];
            }
            return null;
        }

        @Override
        public int nextIndex() {
            return index + 1;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
            removeElementAt(index);
        }

        @Override
        public void set(T t) {
            setElementAt(t, index);
        }

        @Override
        public void add(T t) {
            insertElementAt(t, index);
        }
    }

}

